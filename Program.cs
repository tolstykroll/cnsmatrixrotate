﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cnsMatrixRotate
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // 1 2 3  
            // 8 9 4  
            // 7 6 5  
            Console.Write("Enter your Number:");
            int n = Convert.ToInt32(Console.ReadLine());
            int[,] matrix = new int[n, n];
            int printValue = 1;
            int c1 = 0, c2 = n - 1;
            while (printValue <= n * n)
            {
                //Right Direction Move  
                for (int i = c1; i <= c2; i++)
                    matrix[c1, i] = printValue++;
                //Down Direction Move  
                for (int j = c1 + 1; j <= c2; j++)
                    matrix[j, c2] = printValue++;
                //Left Direction Move  
                for (int i = c2 - 1; i >= c1; i--)
                    matrix[c2, i] = printValue++;
                //Up Direction Move  
                for (int j = c2 - 1; j >= c1 + 1; j--)
                    matrix[j, c1] = printValue++;
                c1++;
                c2--;
            }

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    Console.Write(matrix[i, j] + "\t");
                }
                Console.WriteLine();
            }

            Console.WriteLine();

            // rotate my matrix

            matrix = Rotate(matrix);

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    Console.Write(matrix[i, j] + "\t");
                }
                Console.WriteLine();
            }

            Console.Read();
        }

        public static int[,] Rotate(int[,] m)
        {
            var result = new int[m.GetLength(1), m.GetLength(0)];

            for (int i = 0; i < m.GetLength(1); i++)
                for (int j = 0; j < m.GetLength(0); j++)
                    result[i, j] = m[m.GetLength(0) - j - 1, i];

            return result;
        }

    }
}
